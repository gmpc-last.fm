# Dutch translation for gmpc
# Copyright (c) 2009 Rosetta Contributors and Canonical Ltd 2009
# This file is distributed under the same license as the gmpc package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: gmpc\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2009-03-17 15:41+0100\n"
"PO-Revision-Date: 2009-07-12 16:02+0000\n"
"Last-Translator: Yentl <y.v.t@scarlet.be>\n"
"Language-Team: Dutch <nl@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2009-07-25 15:28+0000\n"
"X-Generator: Launchpad (build Unknown)\n"

#. Fetch artist art
#: ../src/plugin.c:594
msgid "Artist images"
msgstr "Artiest afbeeldingen"

#. Fetch artist text
#: ../src/plugin.c:601
msgid "Artist biography"
msgstr "Artiest biografie"

#. Fetch album art
#: ../src/plugin.c:608
msgid "Similar artists"
msgstr "Gelijksoortige artiesten"

#. Fetch artist art
#: ../src/plugin.c:615
msgid "Album cover"
msgstr "Album cover"

#. Fetch artist art
#: ../src/plugin.c:623
msgid "similar songs"
msgstr "Gelijksoortige liedjes"

#: ../src/plugin.c:664
msgid "Last FM metadata fetcher"
msgstr "Last FM metadata downloader"
